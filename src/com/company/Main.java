package com.company;

public class Main {

    public static void main(String[] args) {
        Contact contact1 = new Contact();
        contact1.setFirstName("John");
        contact1.setLastName("Doe");
        System.out.println(contact1.getFullName()); // Expected : "John Doe"


        Contact contact2 = new Contact("Bob", "McLarson");
        System.out.println(contact2.getFullName()); // Expected : "Bob McLarson"

        contact1.setPhone("+3365487987");
        System.out.println(contact1.getPhoneCountry()); // Expected : "FR"

        contact2.setPhone("+4454654654");
        System.out.println(contact2.getPhoneCountry()); // Expected : "UK"


        System.out.println(contact1.getSummary());
        /* Expected :
            First name : John
            Last name : Doe
            Phone : +3365487987
         */

        System.out.println(contact2.getSummary());
        /* Expected :
            First name : John
            Last name : Doe
            Phone : +4454654654
         */

        // Part 2 : Employees
//        Employee employeeJohn = new Employee("John Doe", 55111);
//        Employee employeeBob = new Employee("Bob Watson", 40212);
//
//        System.out.println("John monthly salary is " + employeeJohn.getMonthlySalary());
//        System.out.println("Bob monthly salary is " + employeeJohn.getMonthlySalary());
//
//        if (employeeJohn.isBetterPaidThan(employeeBob)) {
//            System.out.println("John is better paid than Bob");
//        } else {
//            System.out.println("Bob is better paid than Bob");
//        }
    }
}
